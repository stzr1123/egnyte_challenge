# Egnyte coding challenge

This repository contains my solutions to the pre-interview coding task for the Junior Software Engineer position at Egnyte.

## Setup

* Install Python 3
* Make scripts executable with
```sh
$ chmod u+x tic_tac_toe.py
$ chmod u+x test.py
```
All modules used are part of the Python standard library so there is no need for a virtualenv.

## Example run

```sh
$ ./tic_tac_toe.py < [board.txt]
```

## Unit tests

Unit tests have been added using the unittest framework for Python. The code passes all unit tests. To run unit tests execute,

```sh
$ ./test.py -v
```

Unit tests included are,

* Empty board
* Board with only one element
* Small board
* Large board
* Really large board
* Invalid board (invalid marker)
* Invalid board (invalid dimensions)
* Left diagonal win
* Right diagonal win
* Column win
* Row win
* No winners board

## Algorithmic complexity

(Worse case) Time complexity is O(N^2). Space complexity is O(1).

## Description of algorithm

The important part of the algorithm is to walk down each cell only once and keep track if the row and column (and diagonal) it forms part of could be a potential winning row or column. This can be done by simply keeping track of the overall marker for the row, for example, if that current marker does not match the overall marker, it is not a winning row.

Check rows of the board one by one. As you walk down each column in the row, annotate if that column could be a potential winning column or otherwise. Similarly, if column is part of a diagonal, annotate if that diagonal could be a potential winning diagonal or not. Similarly, annotate if the current row is a potential winning row.

If the row is not a winning row continue with the following row.

After all rows are processed, check if there was any winning column or diagonal. If there was no winning row, column or diagonal, return '.' for no winners.

At each row, assert the length of the row is consistent with the dimensions of the board. At each marker, assert it is a valid marker (i.e. 'X', 'O', '.').

## Author

Esteban Zacharzewski

email: ezachar@uchicago.edu

phone: +49 017 647 167 319
