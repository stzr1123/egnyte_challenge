#!/usr/bin/env python3
###############################################################################
# Unit test module for Tic-Tac-Toe checker.
###############################################################################
import unittest
from tic_tac_toe import TicTacToe


class TicTacToeTest(unittest.TestCase):

    def test_empty_board(self):
        board = []
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, '.')

    def test_one_element(self):
        board = [['X']]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'X')

    def test_small_board(self):
        board = [['X','O','O'],['O','O','X'],['X','O','.']]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'O')

    def test_large_board(self):
        board = []
        for row_num in range(100):
            if row_num == 5:
                row = ['X'] * 100
            else:
                row = ['.'] * 100
            board.append(row)
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'X')

    def test_really_large_board(self):
        board = []
        for row_num in range(10000):
            if row_num == 5:
                row = ['O'] * 9999 + ['.']
            else:
                row = ['X'] * 100 + ['O'] + ['X'] * 9899
            board.append(row)
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'O')

    def test_invalid_marker(self):
        board = [['X','O','O'],['O','O','X'],['X','O','m']]
        N = len(board)
        tester = TicTacToe(board, N)
        with self.assertRaises(SystemExit) as cm:
            winner = tester.find_winner()
        self.assertEqual(cm.exception.code, 1)

    def test_invalid_dimensions(self):
        board = [['X','O','O'],['O','O'],['X','O','X']]
        N = len(board)
        tester = TicTacToe(board, N)
        with self.assertRaises(SystemExit) as cm:
            winner = tester.find_winner()
        self.assertEqual(cm.exception.code, 1)

    def test_no_winners(self):
        board = [['X','O','O'],['O','O','.'],['X','.','X']]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, '.')

    def test_row_winner(self):
        board = [['X','X','O'],['O','O','O'],['X','.','X']]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'O')

    def test_column_winner(self):
        board = [['X','X','O'],['O','X','O'],['O','X','.']]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'X')

    def test_left_diag_winner(self):
        raw = "X... .X.. ..X. ...X"
        board = [[char.upper() for char in row] for row in raw.split()]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'X')

    def test_right_diag_winner(self):
        raw = "  ...O ..O. .O.. O..."
        board = [[char.upper() for char in row] for row in raw.split()]
        N = len(board)
        tester = TicTacToe(board, N)
        winner = tester.find_winner()
        self.assertEqual(winner, 'O')


if __name__ == '__main__':
    unittest.main()
