#!/usr/bin/env python3
###############################################################################
# Tic-Tac-Toe checker. Evaluate winner of a Tic-Tac-Toe game given a NxN board.
###############################################################################
import sys

class TicTacToe(object):
    X = 'X'
    O = 'O'
    dot = '.'
    no_win = '.'
    markers = [X, O, dot]

    def __init__(self, board, N):
        self.N = N
        self.board = board
        self.results = {'row': {},
                        'column': {},
                        'diag1': None,
                        'diag2': None,
                        }

    def _is_left_diag(self, row_num, col_num):
        if row_num == col_num:
            return True
        return False

    def _is_right_diag(self, row_num, col_num):
        num = (self.N - 1) - row_num
        if col_num == num:
            return True
        return False

    def print_board(self):
        for row in self.board:
            s = ''.join(row)
            print(s)

    def find_winner(self):
        # walk board by row
        for row_num, row in enumerate(self.board):
            if len(row) != self.N:
                # board is not NxN dimensions
                sys.exit(1)

            # retrieve row markers
            cur_marker = row[0]
            row_marker = self.results['row'].get(row_num, cur_marker)

            for col_num, col in enumerate(row):
                if not col in self.markers:
                    # marker is not valid marker
                    sys.exit(1)

                # retrieve column markers
                cur_marker = col
                col_marker = self.results['column'].get(col_num, cur_marker)

                # update row information
                if (row_marker != cur_marker) and (row_marker != self.no_win):
                    self.results['row'][row_num] = self.no_win
                    row_marker = self.no_win
                elif row_marker == cur_marker:
                    self.results['row'][row_num] = row_marker

                # update column information
                if (col_marker != cur_marker) and (col_marker != self.no_win):
                    self.results['column'][col_num] = self.no_win
                elif col_marker == cur_marker:
                    self.results['column'][col_num] = col_marker

                # update diagonal information if diagonal
                if self._is_left_diag(row_num=row_num, col_num=col_num):
                    diag = 'diag1'
                elif self._is_right_diag(row_num=row_num, col_num=col_num):
                    diag = 'diag2'
                else:
                    diag = None

                if diag:
                    diag_marker = self.results[diag]
                    if not diag_marker:
                        self.results[diag] = cur_marker
                    elif (diag_marker != cur_marker) and \
                                (diag_marker != self.no_win):
                        self.results[diag] = self.no_win

            # check if row is a winner
            if row_marker != self.no_win:
                return row_marker

        # check if a column is a winner
        for _, marker in self.results['column'].items():
            if marker != self.no_win:
                return marker

        # check if either diagonal is a winner
        for diag in ['diag1', 'diag2']:
            marker = self.results[diag]
            if marker and (marker != self.no_win):
                return marker

        # no winners
        return self.no_win


if __name__ == '__main__':

    inputs = sys.stdin.read().split()
    board = [[char.upper() for char in row] for row in inputs]
    N = len(board)

    tester = TicTacToe(board, N)
    winner = tester.find_winner()
    # tester.print_board()
    # print(N)
    print(winner)
